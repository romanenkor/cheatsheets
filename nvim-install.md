# NeoVim configuration

## What's included?

### Plugins
* Utility
	* `Plug` - Plugin manager
* Visuals
	* `Vim-Smoothie` - Smooth scrolling
	* `Dracula` - solid dark theme
	* `Gruvbox` - solid light theme
	* `DevIcons` - Icons in file manager
	* `Airline` - Convenient status bar
	* `GitGutter` - Preview diff in side column
	* `Vim-Smoothie` - Smooth scrolling
* Functionality
	* `Fugitive` - Git wrapper
	* `NerdTree` - File manager in side bar
	* `NerdTree-git-plugin` - Git status flags/highlights in the file manager
	* `NerdTree-syntax-highlight` - Syntax highlighting in file manager
	* `FZF` - Fuzzy file search
	* `Vim-Editor-Config` - Enforce project formatting between different editors
* Integration
	* `Vim-Tmux-Navigator` - Navigate between Vim and Tmux seamlessly
	* `Vim-Tmux-Focus-Events` - Fix focus events in tmux required by some Vim plugins
* Productivity
	* `Vim-Repeat` - Make . command work for plugins
	* `Vim-Surround` - Add commands to surround text
	* `Vim-Commentary` - Add comment commands
	* `ReplaceWithRegister` - Repeatable replace commands
* Text Objects
    	* `Vim-Indent-Object` - Text object for indentation
	* `Vim-Textobj-Line` - Text object for a line
	* `Vim-Textobj-Entier` - Text object for a line
* Language Support
	* `Conquer of Completion` - An Intellisense engine
	* `Vim-Slime` - REPL for Vim with variety of sinks
	* `Vim-Pandoc` - Easier documents editing with Pandoc
	* `Vim-Markdown-Folding` - Folding for Markdown
	* `Vim-Polyglot` - Syntax highlighting for popular languages
	* `Vim-Slime` - REPL for Vim
	* `Vim-Pandoc` - Better documents with Pandoc
	* `Vim-Pandoc-Syntax` - Syntax for Pandoc
* Fixes
	* `Vim-Tmux-Focus-Events` - Fix focus events when running in tmux

## Installation
1. Install NeoVim
2. Install FZF
3. Install the_silver_searcher
4. Install Plug:
> curl -fLo ~/.vim/autoload/plug.vim --create-dirs \\  
&nbsp;&nbsp;https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
5. Install NerdFonts (or other font with powerline support)
6. Run NeoVim and type:
> :PlugInstall
7. Restart NeoVim
