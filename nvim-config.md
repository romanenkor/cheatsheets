Nvim-Cheatsheet (w/Plugins)
======================
2019-11-24  

Modes
--------------

- normal mode: navigate the structure of the file
- insert mode: editing the file
- visual mode: highlight portions of the file to manipulate at once
- ex mode: command mode

Normal Mode Commands
--------------

# Syntax

- `[count] <action> <text object|motion>` - Repeats the command `count` times
- `.` - Repeats the last command

- General
	- `:h {expression}` - display help for {expression}
	- `:wall` - write all
	- `:e` - edit a file
	- `:normal! {expression}` - execute the {expression} exactly as typed
	- `m{posName}/``{posName}` - mark/goto the caret position with name {posName}

- Movement
	- `hjkl` - instead of arrows
	- `^E` - scroll the window down
	- `^Y` - scroll the window up
	- `^F` - scroll down one page
	- `^B` - scroll up one page
	- `gg` - go to top of file
	- `G` - go to bottom of file
	- `w` - go to the beginning of the next word
	- `b` - go to the beginning of the previous word
	- `e` - go to the end of the next word

- Cursor
	- `H` - move cursor to the top of the window
	- `M` - move cursor to the middle of the window
	- `L` - move cursor to the bottom of the window

- History
	- `u` - undo
	- `ctrl + r` - redo

- Text Editing
	- `dd / yy` - delete/yank the current line
	- `D/C` - delete/change until end of line
	- `^/$` - move to the beginning/end of line
	- `I/A` - move to the beginning/end of line and insert
	- `o/O` - insert new line below/above current line and insert
	- `p/P` - paste the clipboard below/above the current line

## Actions

- `d` - delete (also cut)
- `c` - change (delete, then place in insert mode)
- `y` - yank (copy)
- `v` - visually select
- `>` - indent

## Motions

- `a` - all
- `i` - in
- `f/F` - find forward/backward
- `t/T` - find 'til forward/backward (stop just before it reaches the actual character)
- `/` - search forward (up to the next match)
- `?` - search backward (up to the next match)

## Text Objects

- `w` - words
- `s` - sentences
- `p` - paragraphs
- `t` - tags (html/xml)

## Examples
- `diw` - delete in word
- `caw` - change all word
- `yi)` - yank all text inside parentheses
- `ci)` - change all text inside parentheses
- `di[` - delete all text inside square brackets
- `da[` - delete all text inside square brackets, AND the square brackets
- `dt(space)` - delete until the space
- `va"` - visually select all inside double quotes, including double quotes
- `c/ot` - change all until the next occurrence of 'ot'
- `cc` - change current line
- `c6j` - change 6 lines below (down), including the current line
- `yypVr=` - copy the current line (yy), paste it below (p), select it (V) and replace it with = symbols (r=)
- `%s/#/"/g` - replace all sharps (#) by double quotes (") in the document

For instance ci', change the text inside quotes, then you can repeat this motion again in other locations.


Macros
-----------------

- `q{register}` - Record a macro
- `q` - Stop recording *(again)*
- `@{register}` - Replay macro
- `Ctrl+r{register}` - insert the content of `register` *(Insert mode)*

Additional Commands
-----------------
- Windows
	- `:sp` - split window by drawing an horizontal line
	- `:vsp` - split window by drawing a vertical line
	- `:res {n}` - resize current window so that it spans {n} lines
	- `ctrl+w s` - alias for :sp
	- `ctrl+w v` - alias for :vsp
	- `ctrl+w ctrl+w` - move between the panes
	- `ctrl+w =` - resize all windows to an equal size
	- `ctrl+w o` - open the current window so that it takes the whole screen
	- `ctrl+w c` - close the current window
	- `ctrl+w {h|j|k|l}` - jump to the window on the {left|down|up|right} side
	- `ctrl+w [number]` +/- - increase/decrease the size of the current window by {number} lines
- Registers	
	- `:registers` - list the registers
	- `"{register}yiw` - copy the current word to register {register}
	- `"{register}p` - paste the content from register {register}
- Buffers
	- `:ls` - list the buffers
	- `:ls!` - list the buffers, including hidden buffers
	- `:b{buffer}` - go to buffer {buffer}
	- `:b#` - go to the last buffer
	- `:bd{buffer,...}?` - delete the current buffer, or the specified {buffer}
	- `:22,24bd` - delete buffers 22 to 24
	- `:%bd` - delete all buffers
	- `:find` - go to a buffer
	- `:edit` - edit to a buffer
	- `:bnext` - go to the next file in the buffer
	- `:bn` - go to the next file in the buffer
	- `:bp` - go to the previous file in the buffer
	- `:bufdo {command}` - execute {command} on all files in the buffer
	- `:bufdo g/"\s*Hithere/d` - delete the Hithere expression in all files in the buffer
- Args	
	- `:n` - go to next thing in the arg list
- Tabs:
	- `:tabedit {file}` - open the {file} in a new tab
	- `:tabs` - list all tabs
	- `gt` - go to the next tab
	- `gT` - go to the previous tab
	- `{i}gt` - go to tab number {i}, starting at 1 left to right
- Autocompletion:
	- `Ctrl+x Ctrl+f` - complete path
	- `Ctrl+x Ctrl+p` - complete with word before the cursor (also used to navigate competion suggestions)
	- `Ctrl+x Ctrl+n` - complete with word after the cursor (also used to navigate competion suggestions)
	- `Ctrl+x Ctrl+l` - complete line

Plugins
--------------
- Navigation
	- NERDTree
		- `\N` - open the file manager
		- `i` - open split
		- `q` - quit
		- `s` - open vsplit
		- `cd` - change to the  directory under cursor
		- `C` - change root to the directory under cursor
		- `B` - list the bookmarks
		- `:Bookmark {name}` - create the {name} bookmark
	- FZF: fuzzy file finder
		- `;` - search buffers
		- `ctrl+p` - search files
		- `:Ag` - search in files

- IDE
	- fugitive: git tool
	- Coc: syntax checker / linter

- Motion
	- Surround
		- `ds"` - delete the surrounding quotes
		- `cs"'` - change surrounding double quotes with single quotes
		- `ysiw"` - add surrounding double quotes to the inner word 
		- `csth2` - change the surrounding tag with h2
	- Commentary
		- `toggle` comments
		- `cml` - comment the line to the right (l)
		- `cmj` - comment the line below (j), including the current line
		- `cmip` - comment the paragraph
	- ReplaceWithRegister
		- `griw` replace the inner word with the register
	- Titlecase
		- `gti'` title case inside single quotes
		- `gtip` title case the inner paragraph
	- Sort-motion
		- `gsip` sort the inner paragraph
	- System-copy
		- `cpi'` - copy all inside single quote
		- `cpip` - copy all inside paragraph
- Markdown Folding
	- Essential
		- `zi` - switch folding on or off
		- `za` - toggle current fold open/closed
		- `zc` - close current fold
		- `zR` - open all folds
		- `zM` - close all folds
		- `zv` - expand folds to reveal cursor
	- Navigation
		- `zj` - move down to top of next fold
		- `zk` - move up to bottom of previous fold
- Custom Text Objects
	- Indent
		- `cmii` comment the inner indented section
		- `cmai` comment the inner indented section (and sublevels)
	- Entire
		- `cmae`	comment the entire document
		- `dae`	delete the entire document
	- Line
		- `cil` change inner line (without the leading indentation)

Features of Vim without plugins
=================

- fuzzy file search
- tag jumping
- autocomplete
- file browsing
- snippets
- build integration

Related
-------------
- [vim decent navigation](https://github.com/lingtalfi/vim-decent-navigation)
- [vim survivor kit](https://github.com/lingtalfi/vim-survivor-kit)
- [vim refresher notes](https://github.com/lingtalfi/vim-refresher-notes)

Sources
-------------
- Videos
	- https://www.youtube.com/watch?v=5r6yzFEXajQ
	- https://www.youtube.com/watch?v=XA2WjJbmmoM
	- https://www.youtube.com/watch?v=wlR5gYd6um0
	- Your First Vim Plugin: https://www.youtube.com/watch?v=lwD8G1P52Sk
	- Stephen Belcher - Registers (Intermediate): https://www.youtube.com/watch?v=sf1prtd_2E8
	- Working with many files step 1: https://www.youtube.com/watch?v=5-Uaeps421w
	- Working with many files step 2: https://www.youtube.com/watch?v=NSTSz8riL2M
	- Working with many files step 3: https://www.youtube.com/watch?v=U0CZsdzNJCQ
	- Vim Essential Plugin: NERDTree: https://www.youtube.com/watch?v=CPu9mDpSYj0
- Pages
	- Basic Mapping: http://learnvimscriptthehardway.stevelosh.com/chapters/03.html 
